img2docx - C++ library for creation docx documents without need for MS Word to be installed

img2pptx - C++ library for creation pptx documents without need for MS PowerPoint to be installed

pdfprocessing - C++ pdf editing library based on PoDoFo project (http://podofo.sourceforge.net/)

pdfreader - C++ library and desktop application for PDF viewing based on MuPDF libary (http://www.mupdf.com/)

pptxvideo - C++ DirectShow source filter which reads pptx files, converts them into images and outputs as video stream + player application using the filter

swf - C++ DirectShow transform filter using Flash ActiveX object to decode images from swf files + player application using the filter

swfsrc - C++ DirectShow source filter for processing SWF files

gifenc - Silverlight control with ability of creation of animanted GIF files from a set of images

Img2DocxSlvrt - Silverlight library for creation docx documents without need for MS Word to be installed

Morph - Silverlight control for performing morphing between two images